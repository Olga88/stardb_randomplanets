import React, { useState, useEffect } from 'react';
import Service from '../../services/service';
import CircularProgress from '@material-ui/core/CircularProgress';

import './random-planet.css';

const RandomPlanet = () => {

  const [rndPlanet, setRndPlanet] = useState(null)
  const [image, setImage] = useState(null);

  useEffect(() => {
    new Service().getRndPlanet().then((data) => {
      setRndPlanet(data.results)
    })
  }, [])
  
  function getRndPlanet(){
    const rndIndex = Math.floor(Math.random() * 10)
    rndPlanet === null ? <CircularProgress /> : console.log(rndPlanet[rndIndex])
  }
  setInterval(getRndPlanet, 5000)

  if (rndPlanet) {
    return (
      <div className="random-planet jumbotron rounded">
        {image === null ? <CircularProgress /> :
          <img className="planet-image" alt='planets'
            src={image} />}
        <div>
          <h4>{rndPlanet.name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">Population</span>
              <span>{rndPlanet.population}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Rotation Period</span>
              <span>{rndPlanet.rotation_period}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Diameter</span>
              <span>{rndPlanet.diameter}</span>
            </li>
          </ul>
        </div>
      </div>
    );
  } else {
    return (<div></div>)
  }
}
 export default RandomPlanet;