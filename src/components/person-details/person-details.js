import React, { useContext, useState, useEffect } from 'react';
import { NameContext } from '../app/app';
import Service from '../../services/service';
import CircularProgress from '@material-ui/core/CircularProgress';

import './person-details.css';

const PersonDetails = () => {
  const [information, setInformation] = useState(null)
  const [image, setImage] = useState(null);

  const { profileInformation } = useContext(NameContext);
  useEffect(() => {

    setInformation(profileInformation);

    if (profileInformation !== null) {
      const symbol = profileInformation.url.match(/\d+/);
      const typeSymbol = profileInformation.url.split("/")

      switch (typeSymbol[4]) {
        case 'people':
          new Service()
            .getPeoplesImg(symbol)
            .then((url) => { setImage(url) });
          break;
        case 'planets':
          new Service()
            .getPlanetsImage(symbol)
            .then((url) => { setImage(url) });
          break;
        case 'starships':
          new Service()
            .getStarshipsImg(symbol)
            .then((url) => { setImage(url) });
          break;
        default:
          break;
      }
    }
    return (() => { setImage(null) })
  }, [profileInformation]) //не забывать отслеживать состояние

  if (information) {
    return (
      <div className="person-details card">
        {image === null ? <CircularProgress /> :
          <img className="person-image" alt={information.name}
            src={image} />}

        <div className="card-body">
          <h4>{information.name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">
                {information.gender ? 'Gender' :
                  information.climate ? 'Climate' :
                    information.MGLT ? 'MGLT' : ''}</span>
              <span>
                {information.gender ? information.gender :
                  information.climate ? information.climate : information.MGLT}</span>
            </li>
            <li className="list-group-item">
              <span className="term">
                {information.birth_year ? 'Birth year' :
                  information.gravity ? 'Gravity' :
                    information.crew ? 'Crew' : ''}
              </span>
              <span>
                {information.birth_year ? information.birth_year :
                  information.gravity ? information.gravity : information.crew}</span>
            </li>
            <li className="list-group-item">
              <span className="term">
                {information.eye_color ? 'Eye color' :
                  information.rotation_period ? 'Rotation period' :
                    information.model ? 'Model' : ''}
              </span>
              <span>
                {information.eye_color ? information.eye_color :
                  information.rotation_period ? information.rotation_period : information.model}</span>
            </li>
          </ul>
        </div>
      </div>
    )
  } else {
    return (<div></div>)
  }
}

export default PersonDetails;