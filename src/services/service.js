class Service {
    _link = 'https://swapi.dev/api/';

    async getPeoples() {
        const data = await fetch(`${this._link}people/`);
        if (data.status > 300 || data.status < 199) {
            throw new Error(`Произошла ошибка в запросе ${data.statusText}:${data.status}`);
        }
        return await data.json();
    };

    async getPlanets() {
        const data = await fetch(`${this._link}planets/`);
        if (data.status > 300 || data.status < 199) {
            throw new Error(`Произошла ошибка в запросе ${data.statusText}:${data.status}`);
        }
        return await data.json();
    };

    async getRndPlanet() {
        const data = await fetch(`${this._link}planets/`);
        if (data.status > 300 || data.status < 199) {
            throw new Error(`Произошла ошибка в запросе ${data.statusText}:${data.status}`);
        }
        return await data.json();
    };

    async getStarships() {
        const data = await fetch(`${this._link}starships/`);
        if (data.status > 300 || data.status < 199) {
            throw new Error(`Произошла ошибка в запросе ${data.statusText}:${data.status}`);
        }
        return await data.json();
    };

    _getImage = "https://starwars-visualguide.com/assets/img/";
    
    async getPlanetsImage(id){
        const data = await fetch(`${this._getImage}planets/${id}.jpg`)
        return data.url
    }
    async getPeoplesImg(id){
        const data = await fetch(`${this._getImage}characters/${id}.jpg`)
        return data.url
    }
    async getStarshipsImg(id){
        const data = await fetch(`${this._getImage}starships/${id}.jpg`)
        return data.url
    }

}

export default Service;