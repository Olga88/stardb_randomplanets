import React, { createContext, useState } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';
import Service from '../../services/service';
import ErrorBoundary from '../error_boundary/error-boundary';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";

import './app.css';

const NameContext = createContext();
const App = () => {
  //дынные пользователя (объекта)
  const [profileInformation, setProfileInformation] = useState(null);
 
  //получение данных пользовтеля (объекта)
  const onElementInfo = (value) => {
    setProfileInformation(value);
  }

  return (
    <ErrorBoundary>
    <Router>
      <NameContext.Provider value={{onElementInfo: onElementInfo, profileInformation: profileInformation}}>
        <Header />
        <RandomPlanet />
        <div className="row mb2">
          <Route exact path='/people'>
            <div className="col-md-6">
              <ItemList request={new Service().getPeoples()} />
            </div>
          </Route>
          <Route exact path='/planets'>
            <div className="col-md-6">
              <ItemList request={new Service().getPlanets()} />
            </div>
          </Route>
          <Route exact path='/starships'>
            <div className="col-md-6">
              <ItemList request={new Service().getStarships()} />
            </div>
          </Route>
          <div className="col-md-6">
              <PersonDetails /> {/* отображение данных объекта */}
          </div>
        </div>
      </NameContext.Provider>
    </Router>
    </ErrorBoundary>
  );
};
export { NameContext};
export default App;